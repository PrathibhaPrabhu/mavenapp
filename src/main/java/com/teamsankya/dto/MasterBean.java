package com.teamsankya.dto;

public  class MasterBean {
	
	
private Student_infoBean sbean = new Student_infoBean();
private StudentAddressInfoBean sabean = new StudentAddressInfoBean();
private StudentGuardianInfoBean gbean = new StudentGuardianInfoBean();
public Student_infoBean getInfo() {
	return sbean;
}
public void setInfo(Student_infoBean sbean) {
	this.sbean = sbean;
}
public StudentAddressInfoBean getAddr() {
	return sabean;
}
public void setAddr(StudentAddressInfoBean sabean) {
	this.sabean = sabean;
}
public StudentGuardianInfoBean getGard() {
	return gbean;
}
public void setGard(StudentGuardianInfoBean gbean) {
	this.gbean = gbean;
}
	/*static Student_infoBean info = new Student_infoBean();
	static StudentAddressInfoBean addr = new StudentAddressInfoBean();
	static StudentGuardianInfoBean gard = new StudentGuardianInfoBean();*/
}
