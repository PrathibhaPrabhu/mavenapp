package com.teamsankya.controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.teamsankya.dto.MasterBean;
import com.teamsankya.dto.StudentAddressInfoBean;
import com.teamsankya.dto.StudentGuardianInfoBean;
import com.teamsankya.dto.Student_infoBean;
import com.teamsankya.dao.StudentDao;
import com.teamsankya.util.StuddentServiceManager;




public class CreateStudent extends HttpServlet{
	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		Student_infoBean sbean = new Student_infoBean();
		sbean.setFname(req.getParameter("fname"));
		sbean.setLname(req.getParameter("lname"));
		sbean.setMname(req.getParameter("mname"));
		sbean.setRegno(Integer.parseInt(req.getParameter("regno")));


		StudentAddressInfoBean sabean = new StudentAddressInfoBean();
		sabean.setRegno(Integer.parseInt(req.getParameter("regno")));
		sabean.setAddr1(req.getParameter("address1"));
		sabean.setAddr2(req.getParameter("address2"));
		sabean.setAddr_type(req.getParameter("type"));
		sabean.setAddr_type(req.getParameter("city"));
		sabean.setPincode(Integer.parseInt(req.getParameter("pincode")));


		StudentGuardianInfoBean gbean = new StudentGuardianInfoBean();
		gbean.setRegno(Integer.parseInt(req.getParameter("regno")));
		gbean.setGfname(req.getParameter("guardian first name"));
		gbean.setGmname(req.getParameter("guardian middle name"));
		gbean.setGlname(req.getParameter("guardian last name"));


		MasterBean mbean = new MasterBean();
		mbean.setInfo(sbean);
		mbean.setAddr(sabean);
		mbean.setGard(gbean);
		
		StudentDao dao = StuddentServiceManager
				.getInstence().daoGenarater();		
		dao.createStudent(mbean);
		req.getRequestDispatcher("createResponse.jsp")
		.forward(req, resp);

}
}
